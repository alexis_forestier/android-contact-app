package com.android.contactapp;

import static com.android.contactapp.AddContact._KEY_FROM_ADDC_;
import static com.android.contactapp.MainActivity.RQC_Main;
import static com.android.contactapp.MainActivity._KEY_FROM_MAIN_;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.FileNotFoundException;

public class ProfileContact extends AppCompatActivity {

    public static final int RQC_PROFILE = 102;

    Contact contact;

    TextView name, firstname,gender,birthday,phone,email,address,zip;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_contact);

        this.contact = null;

        this.name = findViewById(R.id.txtv_name);
        this.firstname = findViewById(R.id.txtv_name2);
        this.gender = findViewById(R.id.txtv_name3);
        this.birthday = findViewById(R.id.txtv_name4);
        this.phone = findViewById(R.id.txtv_name5);
        this.email = findViewById(R.id.txtv_name6);
        this.address = findViewById(R.id.txtv_name7);
        this.zip = findViewById(R.id.txtv_name8);

        this.image = findViewById(R.id.imageView2);

        Gson gson = new Gson();
        this.contact = gson.fromJson(getIntent().getStringExtra(_KEY_FROM_MAIN_),Contact.class);

        this.name.setText(this.contact.getName());
        this.firstname.setText(this.contact.getFirstname());
        this.gender.setText(this.contact.getGender());
        this.birthday.setText(this.contact.getBirthdate());
        this.phone.setText(this.contact.getPhoneNumber());
        this.email.setText(this.contact.getEmail());
        this.address.setText(this.contact.getAddress());
        this.zip.setText(this.contact.getZip());
        try {
            this.image.setImageBitmap(this.contact.getImage(ProfileContact.this));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity","onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity","onRestart");
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(ProfileContact.this, MainActivity.class);
        setResult(RESULT_OK, myIntent);
        super.onBackPressed();
    }
}