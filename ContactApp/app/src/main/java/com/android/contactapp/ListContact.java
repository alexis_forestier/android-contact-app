package com.android.contactapp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class ListContact implements Serializable {

    private ArrayList<Contact> contactList;

    public ListContact() {
        this.contactList = new ArrayList<>();
    }

    public void addContact(Contact contact) {
        this.contactList.add(contact);
    }

    public ArrayList<Contact> getContactList() {
        return contactList;
    }

    public ArrayList<String> toArrayListOfString(){
        ArrayList<String> arrayList = new ArrayList<>();
        for(int i=0;i<this.contactList.size();i++){
            arrayList.add(this.contactList.get(i).toString()) ;
        }
        return arrayList;
    }


}
