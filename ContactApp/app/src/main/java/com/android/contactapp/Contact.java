package com.android.contactapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Contact implements Serializable{
    private String imageFileName;

    private String name;
    private  String firstname;
    private String gender;
    private String birthdate;
    private String phoneNumber;
    private String email;
    private String address;
    private String zip;

    public Contact(String name, String firstname, String gender, String birthdate, String phoneNumber, String email, String address, String zip) {
        this.name = name;
        this.firstname = firstname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.zip = zip;

        this.imageFileName = null;
    }

    public Bitmap getImage(Context context) throws FileNotFoundException {
        Bitmap image = BitmapFactory.decodeStream(context.openFileInput(this.imageFileName));
        return image;
    }

    @SuppressLint("SimpleDateFormat")
    public void setImage(Bitmap image,Context context) {
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "myImage" + time;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
            this.imageFileName = fileName;
        }catch (Exception e){
            e.printStackTrace();
            this.imageFileName = null;
        }
    }

    public String getName() {
        return name;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getZip() {
        return zip;
    }

    @NonNull
    @Override
    public String toString() {
        return name + "\n" + firstname + "\n" + phoneNumber;
    }
}
