package com.android.contactapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {

    ArrayList<Contact> arraylist;
    Context context;

    public MyAdapter(ArrayList<Contact> arraylist, Context context) {
        this.arraylist = arraylist;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int i) {
        return arraylist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ConstraintLayout layoutItem;
        LayoutInflater mInflater = LayoutInflater.from(context);
        //(1) : Réutilisation du layout
        if (convertView == null) {
            layoutItem = (ConstraintLayout) mInflater.inflate(R.layout.item_layout, viewGroup, false);
        } else {
            layoutItem = (ConstraintLayout) convertView;
        }

        //(2) : Récupération des TextView de notre layout
        TextView tv = layoutItem.findViewById(R.id.textView);
        ImageView iv = layoutItem.findViewById(R.id.imageView);
        //(3) : Mise à jour des valeurs
        tv.setText(arraylist.get(i).toString());
        try {
            iv.setImageBitmap(arraylist.get(i).getImage(this.context));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //On retourne l'item créé.
        return layoutItem;
    }
}
