package com.android.contactapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddContact extends AppCompatActivity {


    public static final int RQC_AddC = 101;
    public static final int GALLERY_REQUEST = 102;
    private static final int CHOOSE_REQUEST = 103;
    private static final int MY_PERMISSION_REQUEST_CODE_SEND_SMS = 104;
    private static final int STORAGE_PERMISSION_CODE = 300;
    private static final int PHONE_PERMISSION_CODE = 301;

    public static  final String _KEY_FROM_ADDC_ = "kAddC";

    Button buttonConfirm;
    Button btnChangeImage;
    EditText editText;
    TextView txtView;

    String path;

    //Input
    ImageView imageView;
    RadioGroup gender;
    final Calendar myCalendar= Calendar.getInstance();
    EditText birthday;
    EditText email;
    EditText address;
    EditText zip;
    //Required input
    EditText name;
    EditText firstname;
    EditText phone;

    // Intent for result
    Intent intent;
    Contact key_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        this.buttonConfirm = findViewById(R.id.btn_confirm);
        this.txtView = findViewById(R.id.textView);

        //Required input
        this.imageView = findViewById(R.id.img_contact);
        this.name = findViewById(R.id.PersonName);
        this.firstname = findViewById(R.id.PersonFname);
        this.gender = findViewById(R.id.PersonGender);
        this.birthday = findViewById(R.id.PersonBirthday);
        DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateLabel();
            }
        };
        this.birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddContact.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        this.phone = findViewById(R.id.PersonPhone);
        this.email = findViewById(R.id.PersonEmail);
        this.address = findViewById(R.id.PersonAddress);
        this.zip = findViewById(R.id.PersonZip);

        // Intent for result
        this.intent = new Intent(AddContact.this,MainActivity.class);
        this.key_contact = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity","onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity","onRestart");
    }

    @Override
    public void onBackPressed() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int sendSmsPermisson = ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
            if(sendSmsPermisson != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSION_REQUEST_CODE_SEND_SMS
                );
                return;
            }
            this.sendSMS_by_smsManager();
        }

        Gson gson = new Gson();
        String myJson = gson.toJson(this.key_contact);
        this.intent.putExtra(_KEY_FROM_ADDC_, myJson);
        setResult(RESULT_OK, this.intent);
        Log.d("ok",myJson.toString());
        super.onBackPressed();
    }

    private void updateLabel(){
        String myFormat="dd/MM/yy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.FRANCE);
        this.birthday.setText(dateFormat.format(myCalendar.getTime()));
    }

    public String getRadio(){
        if(this.gender.getCheckedRadioButtonId() != -1){
            return ((RadioButton) findViewById(this.gender.getCheckedRadioButtonId())).getText().toString();
        }
        return null;

    }

    public void OnClickValid(View view) {
        if(!this.name.getText().toString().equals("")
                && !this.firstname.getText().toString().equals("")
                && !this.phone.getText().toString().equals("")
        ){
            Contact contact = new Contact(
                    this.name.getText().toString(),
                    this.firstname.getText().toString(),
                     this.getRadio(),
                    this.birthday.getText().toString(),
                    this.phone.getText().toString(),
                    this.email.getText().toString(),
                    this.address.getText().toString(),
                    this.zip.getText().toString()
            );
            contact.setImage(((BitmapDrawable) this.imageView.getDrawable()).getBitmap(),AddContact.this);

            this.key_contact = contact;
            onBackPressed();
        }
        else{
            Toast.makeText(
                    getApplicationContext(),
                    "Please complete the required input !",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public boolean checkPermission(String permission, int requestCode)
    {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(AddContact.this, permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(AddContact.this, new String[] { permission }, requestCode);
            checkPermission(permission,requestCode);
            return false;
        }
        else {
            Toast.makeText(AddContact.this, "Permission already granted", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    public void sendSMS_by_smsManager() {
        String phoneNumber = this.phone.getText().toString();
        String message = "Vous venez d'ajouter " + this.name.getText().toString();

        try {
            SmsManager smsManager = SmsManager.getDefault();

            smsManager.sendTextMessage(phoneNumber,
                    null,
                    message,
                    null,
                    null);
            Toast.makeText(getApplicationContext(),"Your sms has successfully sent!",
                    Toast.LENGTH_SHORT).show();
        }catch (Exception ex) {
            Log.e( "Form","Your sms has failed...", ex);
            Toast.makeText(getApplicationContext(),"Your sms has failed... " + ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }
    }


    @SuppressLint({"IntentReset", "QueryPermissionsNeeded", "SimpleDateFormat"})
    public void OnclickImage(View view){
        if(
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE)
                && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,STORAGE_PERMISSION_CODE)
                && checkPermission(Manifest.permission.READ_PHONE_STATE,PHONE_PERMISSION_CODE)
        ){

            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");

            Intent takeIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            takeIntent.setType("image/*");

            Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if(pictureIntent.resolveActivity(getPackageManager()) != null) {
                try {
                    String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    File photoDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File photoFile = File.createTempFile("photo"+time, ".jpg", photoDir);
                    path = photoFile.getAbsolutePath();
                    Uri photoUri = FileProvider.getUriForFile(AddContact.this, AddContact.this.getApplicationContext().getPackageName()+".provider", photoFile);
                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {takeIntent, pictureIntent});

            startActivityForResult(chooserIntent, CHOOSE_REQUEST);
        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK && requestCode == CHOOSE_REQUEST){

            try {
                path = data.getData().toString();
                this.imageView.setImageURI(Uri.parse(path));
            } catch (Exception e) {
                e.printStackTrace();
                this.imageView.setImageURI(Uri.parse(path)); // same as above

            }

        }
    }
}