package com.android.contactapp;

import static com.android.contactapp.AddContact.RQC_AddC;
import static com.android.contactapp.AddContact._KEY_FROM_ADDC_;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*
* Main Activity
* > Contact list
* */
public class MainActivity extends AppCompatActivity {

    public static final int RQC_Main = 100;

    public static  final String _KEY_FROM_MAIN_ = "kMain";

    ListContact contacts_list;

    ListView lv;

    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.contacts_list = new ListContact();
        this.restore();

        this.lv = findViewById(R.id.ListView);

        this.adapter = new MyAdapter(
                this.contacts_list.getContactList(),
                MainActivity.this
        );

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        Intent myIntent = new Intent(MainActivity.this, ProfileContact.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(adapter.arraylist.get(pos));
                        myIntent.putExtra(_KEY_FROM_MAIN_, myJson);
                        setResult(RESULT_OK, myIntent);
                        startActivity(myIntent);
                    }
                }
        );

        lv.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                        adapter.arraylist.remove(i);
                        save();
                        adapter.notifyDataSetChanged();
                        return true;
                    }
                }
        );


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.save();
        Log.d("MainActivity","onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity","onRestart");
    }

    public void openAddContact(View view){
        Intent myIntent = new Intent(MainActivity.this, AddContact.class);
        //myIntent.putExtra("KEY_FORM_A", this.editText.getText().toString());
        startActivityForResult(myIntent, RQC_AddC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQC_AddC && resultCode == RESULT_OK) {

            //String result = data.getStringExtra("KEY_FROM_B");
            Gson gson = new Gson();
            Contact contactResult = gson.fromJson(data.getStringExtra(_KEY_FROM_ADDC_),Contact.class);
            String result;

            if(contactResult != null){
                this.contacts_list.addContact(contactResult);
                this.save();

                this.adapter.arraylist = this.contacts_list.getContactList();
                this.adapter.notifyDataSetChanged();
                result = "Contact has been added";
            }
            else{
                result = "Add Contact has been canceled";
            }

            Log.d("Add Contact Result" ,result);
        }
    }

    public void save(){
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = openFileOutput("saveFile", Context.MODE_PRIVATE);
            out = new ObjectOutputStream(fos);
            out.writeObject(this.contacts_list);
            out.close();
            fos.close();
            Log.d("succes","save------------------");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("error","save-----------------");
        }
    }

    public void restore(){
        File directory = getFilesDir();
        File file = new File(directory, "saveFile");
        if(file.exists()){
            FileInputStream fis = null;
            ObjectInputStream in = null;
            try {
                fis = openFileInput("saveFile");
                in = new ObjectInputStream(fis);
                this.contacts_list = (ListContact) in.readObject();
                in.close();
                fis.close();
                Log.d("succes","restore-----------------");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("error","restore-----------------");
            }
        }
    }

}